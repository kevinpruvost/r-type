#include "Transform.hpp"

using namespace RType::Common::Engine;

Transform::Transform(REngine& engine)
    : RComponent(engine)
{
}

Transform::~Transform()
{
}

void Transform::Start()
{
}

void Transform::Update()
{
}

void Transform::Tick()
{
}

void Transform::MoveTo(float x, float y, float speedPerSeconds)
{
    vector.MoveTo(x, y, speedPerSeconds);
}

void Transform::UpdateComponent(RType::Common::Udp::Entities::UpdateRequest updateRequest)
{
    vector.position.x = updateRequest.U.transform.positionX;
    vector.position.y = updateRequest.U.transform.positionY;
    vector.size.x = updateRequest.U.transform.sizeX;
    vector.size.y = updateRequest.U.transform.sizeY;
    vector.MoveTo(updateRequest.U.transform.directionX,
                  updateRequest.U.transform.directionY,
                  updateRequest.U.transform.speed);
    hasMoved = updateRequest.U.transform.hasMoved;
    isStatic = updateRequest.U.transform.isStatic;
    parentId = updateRequest.U.transform.parentId;
}

RType::Common::Udp::Entities::UpdateRequest Transform::CreateUpdateRequest()
{
    RType::Common::Udp::Entities::UpdateRequest l;

    l.U.transform.parentId = parentId;
    l.U.transform.isStatic = isStatic;
    l.U.transform.hasMoved = hasMoved;
    l.U.transform.positionX = vector.position.x;
    l.U.transform.positionY = vector.position.y;
    l.U.transform.sizeX = vector.size.x;
    l.U.transform.sizeY = vector.size.y;
    l.U.transform.speed = vector.GetSpeed();

    l.U.transform.directionX = vector.GetDirection().x;
    l.U.transform.directionY = vector.GetDirection().y;

    return l;
}
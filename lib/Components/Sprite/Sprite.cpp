#include "Sprite.hpp"

using namespace RType::Common::Engine;

Sprite::Sprite(REngine& engine)
    : RComponent(engine)
{

}

Sprite::~Sprite()
{
}

void Sprite::UpdateComponent(RType::Common::Udp::Entities::UpdateRequest updateRequest)
{
    spriteName = updateRequest.U.sprite.spriteName;
    color = updateRequest.U.sprite.color;
}

RType::Common::Udp::Entities::UpdateRequest Sprite::CreateUpdateRequest()
{
    RType::Common::Udp::Entities::UpdateRequest l;

    std::memset(&l.U.sprite.spriteName, 0, COMPONENT_NAME_LENGTH);
    std::memcpy(&l.U.sprite.spriteName, spriteName.c_str(), spriteName.size());
    l.U.sprite.color = color;
    return l;
}

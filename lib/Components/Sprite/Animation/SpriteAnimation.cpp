#include "SpriteAnimation.hpp"

using namespace RType::Common::Engine;

SpriteAnimation::SpriteAnimation(REngine& engine)
    : RComponent(engine)
{

}

SpriteAnimation::~SpriteAnimation()
{
}

void SpriteAnimation::Update()
{
    if (boucle == AnimationBoucle::DontStop)
        animationLooped = true;
}

void SpriteAnimation::resumeAnimationLoop()
{
    animationLooped = true;
}

void SpriteAnimation::startAnimationLoop()
{
    animationLooped = true;
}

void SpriteAnimation::stopAndResetAnimation()
{
    animationLooped = false;
}

void SpriteAnimation::stopAnimationLoop()
{
    animationLooped = false;
}

void SpriteAnimation::animateToRight()
{
    sens = AnimationDirection::ToRight;
}

void SpriteAnimation::animateToLeft()
{
    sens = AnimationDirection::ToLeft;
}

void SpriteAnimation::stopAnimationAtFirstFrame()
{
    boucle = AnimationBoucle::StopAtFirstFrame;
}

void SpriteAnimation::stopAnimationAtLastFrame()
{
    boucle = AnimationBoucle::StopAtLastFrame;
}

void SpriteAnimation::startChrono()
{
    chrono.start();
}

float SpriteAnimation::getChronoTime()
{
    return chrono.getPastTimeInSeconds();
}

void SpriteAnimation::UpdateComponent(RType::Common::Udp::Entities::UpdateRequest updateRequest)
{
    animationRate = updateRequest.U.spriteAnimation.animationRate;
    animationLooped = updateRequest.U.spriteAnimation.animationLooped;
    animationIndex = updateRequest.U.spriteAnimation.animationIndex;
    boucle = updateRequest.U.spriteAnimation.boucle;
    sens = updateRequest.U.spriteAnimation.sens;
}

RType::Common::Udp::Entities::UpdateRequest SpriteAnimation::CreateUpdateRequest()
{
    RType::Common::Udp::Entities::UpdateRequest l;

    l.U.spriteAnimation.sens = sens;
    l.U.spriteAnimation.boucle = boucle;
    l.U.spriteAnimation.animationIndex = animationIndex;
    l.U.spriteAnimation.animationLooped = animationLooped;
    l.U.spriteAnimation.animationRate = animationRate;

    return l;
}

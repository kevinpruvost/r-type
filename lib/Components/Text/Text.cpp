#include "Text.hpp"

using namespace RType::Common::Engine;

Text::Text(REngine& engine)
    : RComponent(engine)
{

}

Text::~Text()
{
}

void Text::UpdateComponent(RType::Common::Udp::Entities::UpdateRequest updateRequest)
{
    text = updateRequest.U.text.text;
    fontName = updateRequest.U.text.fontName;
    fontSize = updateRequest.U.text.fontSize;
    fontColor = updateRequest.U.text.fontColor;
}

RType::Common::Udp::Entities::UpdateRequest Text::CreateUpdateRequest()
{
    RType::Common::Udp::Entities::UpdateRequest l;

    l.U.text.fontSize = fontSize;
    l.U.text.fontColor = fontColor;
    std::memset(l.U.text.fontName, 0, COMPONENT_NAME_LENGTH);
    std::memcpy(&l.U.text.fontName, fontName.c_str(), fontName.size());
    std::memset(l.U.text.text, 0, COMPONENT_NAME_LENGTH);
    std::memcpy(&l.U.text.text, text.c_str(), text.size());

    return l;
}

/*
** EPITECH PROJECT, 2020
** r-type
** File description:
** Weapon
*/

#include "Weapon.hpp"

using namespace RType::Common::Engine;

Weapon::Weapon(REngine & engine)
    : RComponent(engine)
{
}

Weapon::~Weapon()
{
}

void Weapon::getDamage(int damage, CharacterCharacteristics & character)
{
    int health = character.getCurrentHealth();

    health - damage > 0 ? character.setCurrentHealth(health - damage) : \
    character.setCurrentHealth(0);
}

void Weapon::inflictDamage(int damage, CharacterCharacteristics & character)
{
    getDamage(damage, character);
}

void Weapon::UpdateComponent(RType::Common::Udp::Entities::UpdateRequest updateRequest)
{
    _fireRate = updateRequest.U.weapon._fireRate;
    _damage = updateRequest.U.weapon._damage;
    _projectileNumber = updateRequest.U.weapon._projectileNumber;
    spriteNameBullet = updateRequest.U.weapon.spriteNameBullet;
    ally = updateRequest.U.weapon.ally;
}

RType::Common::Udp::Entities::UpdateRequest Weapon::CreateUpdateRequest()
{
    RType::Common::Udp::Entities::UpdateRequest l;

    l.U.weapon._fireRate = _fireRate;
    l.U.weapon._damage = _damage;
    l.U.weapon._projectileNumber = _projectileNumber;
    std::memset(&l.U.weapon.spriteNameBullet, 0, COMPONENT_NAME_LENGTH);
    std::memcpy(&l.U.weapon.spriteNameBullet, spriteNameBullet.c_str(), spriteNameBullet.size());
    l.U.weapon.ally = ally;

    return l;
}

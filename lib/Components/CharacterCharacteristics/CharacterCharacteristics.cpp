/*
** EPITECH PROJECT, 2020
** r-type
** File description:
** CharacterCharacteristics
*/

#include "CharacterCharacteristics.hpp"

using namespace RType::Common::Engine;

CharacterCharacteristics::CharacterCharacteristics(REngine & engine)
    : RComponent(engine)
{
}

CharacterCharacteristics::~CharacterCharacteristics()
{
}

void CharacterCharacteristics::UpdateComponent(RType::Common::Udp::Entities::UpdateRequest updateRequest)
{
    _currentHealth = updateRequest.U.characterisrics._currentHealth;
    _maxHealth = updateRequest.U.characterisrics._maxHealth;
    _speed = updateRequest.U.characterisrics._speed;
}

RType::Common::Udp::Entities::UpdateRequest CharacterCharacteristics::CreateUpdateRequest()
{
    RType::Common::Udp::Entities::UpdateRequest l;

    l.U.characterisrics._currentHealth = _currentHealth;
    l.U.characterisrics._maxHealth = _maxHealth;
    l.U.characterisrics._speed = _speed;

    return l;
}

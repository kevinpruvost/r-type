#include "UITransform.hpp"

using namespace RType::Common::Engine;

UITransform::UITransform(REngine& engine)
    : RComponent(engine)
{
}

UITransform::~UITransform()
{
}

void UITransform::Start()
{
}

void UITransform::Update()
{
}

void UITransform::Tick()
{
}

void UITransform::UpdateComponent(RType::Common::Udp::Entities::UpdateRequest updateRequest)
{
    position.x = updateRequest.U.uiTransform.positionX;
    position.y = updateRequest.U.uiTransform.positionY;
    size.x = updateRequest.U.uiTransform.sizeX;
    size.y = updateRequest.U.uiTransform.sizeY;
    centered = updateRequest.U.uiTransform.centered;
}

RType::Common::Udp::Entities::UpdateRequest UITransform::CreateUpdateRequest()
{
    RType::Common::Udp::Entities::UpdateRequest l;

    l.U.uiTransform.positionX = position.y;
    l.U.uiTransform.positionY = position.x;
    l.U.uiTransform.sizeX = size.y;
    l.U.uiTransform.sizeY = size.x;
    l.U.uiTransform.centered = centered;

    return l;
}

/*
** EPITECH PROJECT, 2020
** r-type
** File description:
** Audio
*/

#include "Audio.hpp"

using namespace RType::Common::Engine;

Audio::Audio(REngine & engine)
    : RComponent(engine)
{
}

Audio::~Audio()
{
}

void Audio::setPitch(const float pitc)
{
    pitch = pitc;
}

void Audio::setVolume(unsigned short volum)
{
    volume = volum;

    if (volume > 100)
        volume = 100;
}

void Audio::UpdateComponent(RType::Common::Udp::Entities::UpdateRequest updateRequest)
{
    audioName = updateRequest.U.audio.audioName;
    isLooped = updateRequest.U.audio.isLooped;
    isPlaying = updateRequest.U.audio.isPlaying;
    isPaused = updateRequest.U.audio.isPaused;
    isStoped = updateRequest.U.audio.isStoped;
    pitch = updateRequest.U.audio.pitch;
    volume = updateRequest.U.audio.volume;
}

RType::Common::Udp::Entities::UpdateRequest Audio::CreateUpdateRequest()
{
    RType::Common::Udp::Entities::UpdateRequest l;

    std::memset(&l.U.audio.audioName, 0, COMPONENT_NAME_LENGTH);
    std::memcpy(&l.U.audio.audioName, audioName.c_str(), audioName.size());
    l.U.audio.isLooped = isLooped;
    l.U.audio.isPlaying = isPlaying;
    l.U.audio.isPaused = isPaused;
    l.U.audio.isStoped = isStoped;
    l.U.audio.pitch = pitch;
    l.U.audio.volume = volume;

    return l;
}